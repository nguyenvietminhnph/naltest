package minh.com.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;

public class DataUtil {

    public static boolean hasMember(JsonObject json, String memberName) {
        return json.has(memberName);
    }

    public static String getJsonString(JsonObject json, String memberName) {
        return getJsonString(json, memberName, null);
    }

    public static Integer getJsonInteger(JsonObject json, String memberName) {
        return getJsonInteger(json, memberName, null);
    }

    public static String getJsonString(JsonObject json, String memberName, String defaultValue) {
        if (!json.has(memberName) || json.get(memberName).isJsonNull()) {
            return defaultValue;
        }
        return json.get(memberName).getAsString().trim();
    }

    public static Integer getJsonInteger(JsonObject json, String memberName, Integer defaultValue) {
        if (!json.has(memberName) || json.get(memberName).isJsonNull()) {
            return defaultValue;
        }
        try {
            return json.get(memberName).getAsInt();
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static boolean isEmpty(String value) {
        if (null == value || 0 == value.length()) {
            return true;
        }
        return false;
    }

    public static boolean isDate(String value, String sPattern) {
        if (isEmpty(value)) {
            return true;
        }
        SimpleDateFormat format = new SimpleDateFormat(sPattern);
        format.setLenient(false);
        try {
            format.parse(value);
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }

    public static Date convertStringToDate(String dateString, String patten) {
        SimpleDateFormat originalFormat = new SimpleDateFormat(patten);
        Date date = null;
        try {
            date = originalFormat.parse(dateString);
        } catch (Exception ex) {
            return null;
        }
        return date;
    }

}
