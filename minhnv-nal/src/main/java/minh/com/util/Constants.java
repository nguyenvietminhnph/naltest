package minh.com.util;

public class Constants {

    public static final String PROPERTIE_MESSAGE = "message";

    public static final Integer DEL_FLG_0 = 0;
    public static final String DEL_FLG_1 = "1";

    public static final String ASC = "asc";
    public static final String DESC = "desc";

    public static final int CODE_500 = 500;
    public static final int CODE_400 = 400;
    public static final String ID_MSE00001 = "MSE00001";
    public static final String ID_MSE00002 = "MSE00002";

}
