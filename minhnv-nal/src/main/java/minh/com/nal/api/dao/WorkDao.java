package minh.com.nal.api.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minh.com.nal.api.entity.WorkEntity;

@Repository
public interface WorkDao extends JpaRepository<WorkEntity, Integer> {

    List<WorkEntity> getAllByDelFlg(Integer delFlg0, Pageable firstPageWithSize);

    WorkEntity findByIdAndDelFlg(Integer id, Integer delFlg0);

}
