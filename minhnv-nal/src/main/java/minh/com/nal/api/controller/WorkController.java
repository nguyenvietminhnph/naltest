package minh.com.nal.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import minh.com.common.ResultJson;
import minh.com.common.WorkException;
import minh.com.nal.api.io.WorkReqInDto;
import minh.com.nal.api.service.WorkService;
import minh.com.util.Constants;
import minh.com.util.MessageUtils;

@RestController
@RequestMapping(value = "api")
public class WorkController {

    private final static Logger logger = LoggerFactory.getLogger(WorkController.class);

    @Autowired
    private WorkService workSevice;

    /**
     * GetWork
     * 
     * @param workId
     * @return ResultJson
     */
    @GetMapping(value = "/list")
    public ResponseEntity<ResultJson> getWork(@RequestParam(value = "page") Integer page,
            @RequestParam(value = "size") Integer size, @RequestParam(value = "field_sort") Integer fieldSort,
            @RequestParam(value = "type_sort") String typeSort) {
        logger.info("##### getWork() START #####");
        ResultJson resultJson;
        try {
            resultJson = workSevice.getWork(page, size, fieldSort, typeSort);
            logger.info("##### getWork() END #####");
            return ResponseEntity.ok(resultJson);
        } catch (Exception ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.internalServerError().body(ResultJson.error(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    Constants.ID_MSE00001, MessageUtils.getMessage(Constants.ID_MSE00001)));
        }
    }

    /**
     * addWork
     * 
     * @param reqInDto
     * @return ResultJson
     */
    @PostMapping(value = "/work")
    public ResponseEntity<ResultJson> addWork(@RequestBody(required = false) WorkReqInDto reqInDto) {
        logger.info("##### addWork() START #####");
        ResultJson resultJson;
        try {
            resultJson = workSevice.saveWork(reqInDto);
            logger.info("##### addWork() END #####");
            return ResponseEntity.ok(resultJson);
        } catch (WorkException ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.badRequest()
                    .body(ResultJson.error(ex.getStatus(), ex.getErrorCode(), ex.getMessage()));
        } catch (Exception ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.internalServerError().body(ResultJson.error(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    Constants.ID_MSE00001, MessageUtils.getMessage(Constants.ID_MSE00001)));
        }
    }

    /**
     * updateWork
     * 
     * @param reqInDto
     * @return ResultJson
     */
    @PutMapping(value = "/work")
    public ResponseEntity<ResultJson> updateWork(@RequestBody(required = false) WorkReqInDto reqInDto) {
        logger.info("##### updateWork() START #####");
        ResultJson resultJson;
        try {
            resultJson = workSevice.saveWork(reqInDto);
            logger.info("##### updateWork() END #####");
            return ResponseEntity.ok(resultJson);
        } catch (WorkException ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.badRequest()
                    .body(ResultJson.error(ex.getStatus(), ex.getErrorCode(), ex.getMessage()));
        } catch (Exception ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.internalServerError().body(ResultJson.error(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    Constants.ID_MSE00001, MessageUtils.getMessage(Constants.ID_MSE00001)));
        }
    }

    /**
     * deleteWork
     * 
     * @param id
     * @return ResultJson
     */
    @DeleteMapping("/work/{id}")
    public ResponseEntity<ResultJson> deleteWork(@PathVariable("id") Integer id) {
        logger.info("##### deleteWork() START #####");
        ResultJson resultJson;
        try {
            resultJson = workSevice.deleteWork(id);
            logger.info("##### deleteWork() END #####");
            return ResponseEntity.ok(resultJson);
        } catch (WorkException ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.badRequest()
                    .body(ResultJson.error(ex.getStatus(), ex.getErrorCode(), ex.getMessage()));
        } catch (Exception ex) {
            logger.info("Error message: " + ex.getMessage());
            return ResponseEntity.internalServerError().body(ResultJson.error(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    Constants.ID_MSE00001, MessageUtils.getMessage(Constants.ID_MSE00001)));
        }
    }
}
