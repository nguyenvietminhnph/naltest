package minh.com.nal.api.io;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
public class WorkResOutDto {

    @JsonProperty("list_work")
    List<WorkOutDto> listWork;
}
