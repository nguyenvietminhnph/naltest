package minh.com.nal.api.io;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
public class WorkOutDto {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("work_name")
    private String workName;

    @JsonProperty("starting_date")
    private Date startingDate;

    @JsonProperty("end_date")
    private Date endDate;

    @JsonProperty("status")
    private Integer status;
}
