package minh.com.nal.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import minh.com.common.ErrorBean;
import minh.com.common.ResultJson;
import minh.com.common.WorkException;
import minh.com.enums.SortEnum;
import minh.com.nal.api.dao.WorkDao;
import minh.com.nal.api.entity.WorkEntity;
import minh.com.nal.api.io.WorkReqInDto;
import minh.com.nal.api.io.WorkResOutDto;
import minh.com.nal.api.io.WorkOutDto;
import minh.com.util.Constants;
import minh.com.util.MessageUtils;

@Service
public class WorkService {

    private final static Logger logger = LoggerFactory.getLogger(WorkService.class);

    @Autowired
    private WorkDao workDao;

    /**
     * getWork
     * 
     * @param page
     * @param size
     * @param fieldSort
     * @param typeSort
     * @return resultJson
     */
    public ResultJson getWork(Integer page, Integer size, Integer fieldSort, String typeSort) {
        logger.info("##### getWork() START #####");
        Pageable firstPageWithSize = PageRequest.of(page, size);

        List<WorkEntity> listWorkEntity = workDao.getAllByDelFlg(Constants.DEL_FLG_0, firstPageWithSize);
        // sort by work name
        if (SortEnum.BY_NAME.getCode().equals(fieldSort)) {
            if (Constants.ASC.equals(typeSort)) {
                listWorkEntity = listWorkEntity.stream()
                        .sorted((o1, o2) -> o1.getWorkName().compareTo(o2.getWorkName())).collect(Collectors.toList());
            }
            if (Constants.DESC.equals(typeSort)) {
                listWorkEntity = listWorkEntity.stream()
                        .sorted((o1, o2) -> o2.getWorkName().compareTo(o1.getWorkName())).collect(Collectors.toList());
            }
        }

        // sort by starting date
        if (SortEnum.BY_START_DATE.getCode().equals(fieldSort)) {
            if (Constants.ASC.equals(typeSort)) {
                listWorkEntity = listWorkEntity.stream()
                        .sorted((o1, o2) -> o1.getStartingDate().compareTo(o2.getStartingDate()))
                        .collect(Collectors.toList());
            }
            if (Constants.DESC.equals(typeSort)) {
                listWorkEntity = listWorkEntity.stream()
                        .sorted((o1, o2) -> o2.getStartingDate().compareTo(o1.getStartingDate()))
                        .collect(Collectors.toList());
            }
        }
        // sort by end date
        if (SortEnum.BY_END_DATE.getCode().equals(fieldSort)) {
            if (Constants.ASC.equals(typeSort)) {
                listWorkEntity = listWorkEntity.stream().sorted((o1, o2) -> o1.getEndDate().compareTo(o2.getEndDate()))
                        .collect(Collectors.toList());
            }
            if (Constants.DESC.equals(typeSort)) {
                listWorkEntity = listWorkEntity.stream().sorted((o1, o2) -> o2.getEndDate().compareTo(o1.getEndDate()))
                        .collect(Collectors.toList());
            }
        }
        List<WorkOutDto> listResOut = convertWorkEntityToWorkRes(listWorkEntity);
        WorkResOutDto resOutDto = new WorkResOutDto();
        resOutDto.setListWork(listResOut);
        logger.info("##### getWork() END #####");
        ResultJson resultJson = new ResultJson(HttpStatus.OK.value(), resOutDto);
        return resultJson;
    }

    /**
     * Save Work
     * 
     * @param reqInDto
     * @return resultJson
     * @throws WorkException
     */
    public ResultJson saveWork(WorkReqInDto reqInDto) throws WorkException {
        logger.info("##### saveWork() START #####");
        WorkEntity workEntity = null;
        // check exist for updating
        if (Objects.nonNull(reqInDto.getId())) {
            workEntity = workDao.findByIdAndDelFlg(reqInDto.getId(), Constants.DEL_FLG_0);
            if (Objects.isNull(workEntity)) {
                logger.info("##### WorkEntity is null #####");
                throw new WorkException(Constants.CODE_400, Constants.ID_MSE00002,
                        MessageUtils.getMessage(Constants.ID_MSE00002, reqInDto.getId()));
            }
        }
        workEntity = convertWorkRepToEntity(reqInDto);
        workEntity.setDelFlg(0);
        workDao.save(workEntity);
        logger.info("##### saveWork() END #####");
        ResultJson resultJson = new ResultJson(HttpStatus.OK.value(), workEntity);
        return resultJson;
    }

    /**
     * deleteWork
     * 
     * @param id
     * @return resultJson
     * @throws WorkException
     */
    public ResultJson deleteWork(Integer id) throws WorkException {
        logger.info("##### deleteWork() START #####");
        WorkEntity workEntity = workDao.findByIdAndDelFlg(id, Constants.DEL_FLG_0);
        if (Objects.isNull(workEntity)) {
            logger.info("##### WorkEntity is null #####");
            throw new WorkException(Constants.CODE_400, Constants.ID_MSE00002,
                    MessageUtils.getMessage(Constants.ID_MSE00002, id));
        }
        workEntity.setDelFlg(1);
        workDao.save(workEntity);
        logger.info("##### deleteWork() END #####");
        ResultJson resultJson = new ResultJson(HttpStatus.OK.value(), new ArrayList<ErrorBean>());
        return resultJson;
    }

    /**
     * convertWorkRepToEntity
     * 
     * @param reqInDto
     * @return workEntity
     */
    private WorkEntity convertWorkRepToEntity(WorkReqInDto reqInDto) {
        WorkEntity workEntity = new WorkEntity();
        workEntity.setId(reqInDto.getId());
        workEntity.setWorkName(reqInDto.getWorkName());
        workEntity.setStartingDate(reqInDto.getStartingDate());
        workEntity.setEndDate(reqInDto.getEndDate());
        workEntity.setStatus(reqInDto.getStatus());
        return workEntity;
    }

    /**
     * convertWorkEntityToWorkRes
     * 
     * @param workEntity
     * @return resOutDto
     */
    private List<WorkOutDto> convertWorkEntityToWorkRes(List<WorkEntity> listWorkEntity) {
        List<WorkOutDto> listWorkOut = new ArrayList<WorkOutDto>();
        for (WorkEntity workEntity : listWorkEntity) {
            WorkOutDto resOutDto = new WorkOutDto();
            resOutDto.setId(workEntity.getId());
            resOutDto.setWorkName(workEntity.getWorkName());
            resOutDto.setStartingDate(workEntity.getStartingDate());
            resOutDto.setEndDate(workEntity.getEndDate());
            resOutDto.setStatus(workEntity.getStatus());
            listWorkOut.add(resOutDto);
        }
        return listWorkOut;
    }
}
