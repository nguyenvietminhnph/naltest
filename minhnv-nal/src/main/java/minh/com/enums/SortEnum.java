package minh.com.enums;

public enum SortEnum {

    BY_NAME(1, "BY_NAME"), BY_START_DATE(2, "BY_START_DATE"), BY_END_DATE(3, "BY_END_DATE");

    private Integer code;
    private String status;

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    SortEnum(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public static String getTaskPriority(Integer code) {
        for (SortEnum e : SortEnum.values()) {
            if (e.code.equals(code))
                return e.name();
        }
        return "";
    }
}
