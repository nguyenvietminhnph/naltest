package minh.com.enums;

public enum StatusEnum {

    PLANNING(1, "PLANING"), DOING(2, "DOING"), COMPLETE(3, "COMPLETE");

    private Integer code;
    private String status;

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    StatusEnum(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public static String getTaskPriority(Integer code) {
        for (StatusEnum e : StatusEnum.values()) {
            if (e.code.equals(code))
                return e.name();
        }
        return "";
    }
}
