package minh.com.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResultJson implements Serializable {

    private static final long serialVersionUID = 1432849570656112368L;

    private int status;

    private List<ErrorBean> errors;

    private Object result;

    public ResultJson(Object result) {
        this.result = result;
    }

    public ResultJson() {
    }

    public ResultJson(int status, List<ErrorBean> errors) {
        this.status = status;
        this.errors = errors;
    }

    public ResultJson(int status, Object result) {
        this.errors = new ArrayList<ErrorBean>();
        this.status = status;
        this.result = result;
    }

    public static ResultJson error(int status, String code, String message) {
        List<ErrorBean> errors = new ArrayList<ErrorBean>();
        ErrorBean errorBean = new ErrorBean(code, message);
        errors.add(errorBean);
        return new ResultJson(status, errors);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ErrorBean> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorBean> errors) {
        this.errors = errors;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

}
